import './footer.css';
import ad1 from '../../media/link.png'
import ad2 from '../../media/link3.png'
import Text from '../text/text'
import Hint from '../hint/hint'
import Heading from '../heading/heading'
import { useTranslation, Trans } from "react-i18next"


function Footer() {
    const { t, i18n } = useTranslation();

    return (
        <div className="Footer">
            <Heading>🖖 </Heading>
            <Hint>{t('Создайте удобные ссылки на мероприятие в Clubhouse в один клик')}</Hint>
            <Text>{t('Связаться с нами')}</Text>
            <div style={{marginTop: 20}}/>
            <a href="https://yadda.io" target="_blank"><img className="Footer__ad" src={ad1}></img></a>
            <a href="https://t.me/moskovskiy" target="_blank"><img className="Footer__ad" src={ad2}></img></a>
            <div style={{marginBottom: 20}}/>
        </div>
    );
}

export default Footer;
