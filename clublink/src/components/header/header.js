import './header.css';

import logo from '../../media/logo.png'
import { useTranslation, Trans } from "react-i18next";

function Header() {
    const { t, i18n } = useTranslation();
    
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
        <div className="Header">
            <div className="Header__left">
                <a href="/">
                    <img src={logo} className="Header__logo"/>
                    <div className="Header__name">GetClub.link</div>
                </a>
            </div>

            <div className="Header__right">
                <a href="/"><div className="Header__link">{t('Создать')}</div></a>
            </div>
        </div>
    );
}

export default Header;
