import './heading.css';

function Heading(props) {
  return (
    <div className={(props.maxi)?"Heading Heading--max":"Heading"}>
        {props.children}
    </div>
  );
}

export default Heading;
