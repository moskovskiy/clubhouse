import './hint.css';

function Hint(props) {
  return (
    <div className="Hint">
        {props.children}
    </div>
  );
}

export default Hint;
