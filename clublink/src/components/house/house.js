import './house.css';

function House(props) {
  return (
    <div className="House">
        
        <div className="House__time">{props.time}</div>
        <div className="House__header">{props.header}</div>

        {false && <div className="House__house">
            <svg style={{marginRight: 5}} width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M0.7 10.6932C0.313401 10.6932 0 10.3798 0 9.9932V4.92445C0 4.70843 0.0997309 4.50452 0.270241 4.3719L4.57024 1.02746C4.82302 0.830852 5.17698 0.830852 5.42976 1.02746L9.72976 4.3719C9.90027 4.50452 10 4.70843 10 4.92445V9.9932C10 10.3798 9.6866 10.6932 9.3 10.6932L6.7 10.6932C6.3134 10.6932 6 10.3798 6 9.9932V7.3932C6 7.0066 5.6866 6.6932 5.3 6.6932H4.7C4.3134 6.6932 4 7.0066 4 7.3932V9.9932C4 10.3798 3.6866 10.6932 3.3 10.6932L0.7 10.6932Z" fill="#27AE60"></path>
            </svg>
            {props.from}
        </div>}
        
        <div className="House__avatars">
            {props.avatars.map((el) => <>
                <img className="House__avatar" src={el}/>
            </>)}
        </div>
        
        <div className="House__members"><i>{"w/ " + props.members}</i></div>

        <div className="House__description">{props.description}</div>
    </div>
  );
}

export default House;

/*

avatars={["https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56","https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56","https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56","https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56","https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56","https://clubhouseprod.s3.amazonaws.com/616_3bc1048a-806b-4d1b-aba7-c604bd522e56"]}
time="SUN Jan 31, 10 PM PST"
header
from
members="w/ Sriram Krishnan, Marc Andreessen, Steven Sinofsky, Aarthi Ramamurthy, Elon Musk, Garry Tan 🍔"
description="Special episode of Good Time with Elon Musk."/>
*/