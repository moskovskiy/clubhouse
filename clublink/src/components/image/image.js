import './image.css';

function Image(props) {
  return (
    <img src={props.src} className="Image">
    </img>
  );
}

export default Image;
