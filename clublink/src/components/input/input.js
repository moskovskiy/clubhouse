import React from 'react'
import './input.css'

function Input(props) {

    function checkEnter (e) {
        if (e.key === 'Enter') {
            props.onClick()
        }
    }
    
    return (
        <div className="Input">
            <input onKeyDown={checkEnter} tabIndex="0" autoFocus={props.autoFocus} pattern={props.pattern} placeholder={props.hint} value={props.value} onChange={props.onChange} className={(props.action != "")? "Input__field" : "Input__field Input__field--wide"}/>
            {(props.action != "") && <button onClick={props.onClick} className="Input__action">{props.action}</button>}
        </div>
    )
}
  
export default Input;
  