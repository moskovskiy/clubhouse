import './isle.css';

function Isle (props) {
  return (
    <span className="Isle">
        {props.children}
    </span>
  );
}

export default Isle;
