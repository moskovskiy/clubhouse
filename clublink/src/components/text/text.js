import './text.css';

function Text(props) {
  return (
    <div className="Text">
        {props.children}
    </div>
  );
}

export default Text;
