import './view.css';

function View(props) {

    if (props.title) {
        document.title = props.title + " – GetClub.link"
    }

    document.getElementsByTagName('meta')["description"].content = props.description
    
    return (
        <div className="View">
            {props.children}
        </div>
    );
}

export default View;
