import React, { useEffect } from 'react'

import Heading from '../components/heading/heading'
import Input from '../components/input/input'
import Image from '../components/image/image'
import Text from '../components/text/text'
import Isle from '../components/isle/isle'
import View from '../components/view/view'
import Social from '../components/social/social'
import Hint from '../components/hint/hint'
import Divider from '../components/divider/divider'
import { useTranslation, Trans } from "react-i18next"
import { useState } from 'react'
import axios from 'axios'

import img_old from '../media/new.png'
import img_new from '../media/old.png'

function Landing() {
    const { t, i18n } = useTranslation();
  
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };

    let [wrong, setWrong] = useState(false)
    let [link, setLink] = useState("")
    let [hint, setHint] = useState(t('👋  / Ссылка из Clubhouse'))
    let [btnText, setBtn] = useState("")

    function updateLink (e) {
        setLink(e.target.value)
        setWrong(false)
        if(e.target.value != "") {
            setBtn(t('Улучшить'))
        } else {
            setBtn("")
            //setBtn(t('Вставить'))
        }
    }

    function generateLink() {
        if(link != "") {
            axios.post('https://api.getclub.link/api/v1/cover', {
                link: link,
                type:"to"
            }).then((response) => {
                localStorage.setItem('created', "true")
                window.location.href = response.data.covered
            })
            .catch(function (error) {
                setWrong(true)
                //alert(JSON.stringify(error))
            })
        } else {
            let req = async (dispatch) => {
                const text = await navigator.clipboard.readText()
                link = text
                setHint(text)
            }
            req()
        }
    }
    
    function onKeyDownHandler (e) {
        /*alert(JSON.stringify(e))
        if (e.key === 13) {
            generateLink();
        }*/
    };

    return (
    <View>
        <Heading maxi={true}>{t('Правильные ссылки для вашей комнаты в Clubhouse')}</Heading>
        <Text>{t('Создавайте ссылки, которыми круто делиться в соцсетях!')}</Text>
        <Input type="submit" onKeyDown={() => onKeyDownHandler()} value={link} onChange={updateLink} autoFocus={true} hint={hint} action={btnText}
            onClick={() => generateLink()}
        />
        {wrong && <>
            <Divider/>
            <Hint>{t('Некорректная ссылка на событие в Clubhouse')}</Hint>
        </>}

        <Divider/>
        <Divider/>
        <Divider/>

        <Heading>{t('Получайте больше переходов с информативными превью')}</Heading>
        <Text>{t('Описание, спикеры и даты мероприятия отображаются на предпросмотре в соцсетях, вовлекая аудиторию')}</Text>
        <Divider/>
        
        <Isle>
            <Hint>{t('Обычная ссылка 🤔️')}</Hint>
            <Image src={img_new}></Image>
            <Divider/>
        </Isle>

        <Isle>
            <Hint>{t('Что вы получите! 😎 ')}</Hint>
            <Image src={img_old}></Image>
        </Isle>

        <Divider/>
        <Heading>{t('Приятно делиться в соц. сетях')}</Heading>
        <Text>{t('Никакого копипаста, удобные ссылки, чтобы поделиться')}</Text>
        <Social/>
        <Divider/>
        <Text>
            {t('+ Отображается название мероприятия')}<br/>
            {t('+ Отображается дата и время события')}<br/>
            {t('+ Отображаются ваши спикеры с фото')}<br/>
            {t('+ Отображаем события в правильном часовом поясе')}
        </Text>

        <Divider/>
        <Divider/>
        <Divider/>
        <Heading>{t('И многое другое в скором времени...')}</Heading>
    </View>
  );
}

export default Landing;
