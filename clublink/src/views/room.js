import React from 'react'
import MetaTags from 'react-meta-tags'
import Heading from '../components/heading/heading'
import Input from '../components/input/input'
import Text from '../components/text/text'
import Button from '../components/button/button'
import View from '../components/view/view'
import Social from '../components/social/social'
import House from '../components/house/house'
import Hint from '../components/hint/hint'
import Download from '../components/download/download'
import Divider from '../components/divider/divider'
import { useTranslation, Trans } from "react-i18next";
import { useState, useEffect } from 'react' 
import axios from 'axios'
import { Helmet } from "react-helmet"

function timify (ts) {
  /*var date = new Date(unix_timestamp * 1000)
  var day = date.getDay()
  var month = date.getMonth()
  var hours = date.getHours()
  var weekday = date.toLocaleString()
  var minutes = "0" + date.getMinutes()

  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  
  console.log(formattedTime);*/
  return new Date(ts * 1000).toUTCString().substr(0, 22)
}

function Room(props) {
  const { t, i18n } = useTranslation()
  let [wrong, setWrong] = useState(true)
  let [success, setSuccess] = useState(false)

  let [event, setEvent] = useState(false)
  const link = window.location.href.replace(/^(?:\/\/|[^/]+)*\//, '')

  useEffect(() => {
    
    if (localStorage.getItem('created') == "true") {
      localStorage.setItem('created', "")
      setSuccess(true)
    }

    axios.get('https://api.getclub.link/api/v1/info/to/' + props.match.params.id).then((response) => {
        console.log(JSON.stringify(response))
        setEvent(response.data)
        setWrong(true)
    })
    .catch(function (error) {
        setWrong(false)
        //alert(JSON.stringify(error))
    })

  }, [])

  return (
      <>{(wrong && event.data) ? <View title={"Test"}>
          {success && <>
            <Heading>🖖 {t('Ваша ссылка готова!')}</Heading>
              <Input value={window.location.href} action={t('Скопировать')} onClick={navigator.clipboard.writeText(window.location.href)}></Input>
              <Divider/>
              <Hint>{t('Поделиться')}</Hint>
              <Social selling={false}/>
              <Divider/>
            </>
          }
          <House
            avatars={event.data.pics}
            time={timify(event.data.dtstart)}
            header={event.data.title}
            from="GOOD TIME"
            members={event.data.guests}
            description={event.data.description}
          />
          <a href={"https://joinclubhouse.com/event/"+event.data.hash}><Button>{t('Открыть в приложении')}</Button></a>
          {!success && <>
              <Divider/>
              <Hint>{t('Поделиться')}</Hint>
              <Social selling={false}/>
          </>}
          <Download/>
          <Divider/>

          {/*<MetaTags>
            <title>Page 1</title>
            <meta id="meta-description" lang="en" content={"Clubhouse event: " + event.data.description} />
            <meta id="meta-description" lang="ru" content={"Событие в Clubhouse: " + event.data.description} />
            <meta id="og-title" property="og:title" content={t('Событие в Clubhouse') + " " + event.data.title}/>
            <meta id="og-image" property="og:image" content={"https://api.getclub.link/https://api.getclub.link/preview/to/"+link} />
          </MetaTags>*/}
          <Helmet>
            <title lang="en">{"Clubhouse event: " + event.data.title + " – GetClub.link"}</title>
            <title lang="ru">{"Событие в Clubhouse: " + event.data.title + " – GetClub.link"}</title>

            <meta name="description" content={event.data.description} />

            <meta id="og-title" property="og:title" content={t('Событие в Clubhouse') + " " + event.data.title + " – GetClub.link"}/>
            <meta id="og-image" property="og:image" content={"https://api.getclub.link/https://api.getclub.link/preview/to/" + link} />
          </Helmet>

      </View> : <View>
            <House
            avatars={[]}
            time={"..."}
            header={"..."}
            from="..."
            members={"..."}
            description={"..."}/>
            <Social selling={true}/>
            <Download/>
          <View/>
      </View>}</>
  );
}

export default Room;
