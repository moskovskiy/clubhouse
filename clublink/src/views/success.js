import React from 'react'

import Heading from '../components/heading/heading'
import Input from '../components/input/input'
import Text from '../components/text/text'
import Isle from '../components/isle/isle'
import View from '../components/view/view'
import Social from '../components/social/social'
import Hint from '../components/hint/hint'

function Success() {
  return (
    <View>
        <Heading></Heading>
    </View>
  );
}

export default Success;
